require("dotenv").config();
const express = require("express");
const routes = require("./src/routes/Routes"); // Assuming this is where your routes are defined
const errorHandler = require("./src/middlewares/errorHandler"); // Custom error handler
const cors = require("cors");

const app = express();
const port = process.env.PORT || 7000;

// CORS configuration to allow all domains (could be restrictive for production)
app.use(
  cors({
    origin: "*", // Allow all domains (you can replace "*" with specific domains if needed)
    methods: "GET,POST,PUT,DELETE", // Allowed HTTP methods
    allowedHeaders: "Content-Type,Authorization", // Allowed headers
  })
);

// Middleware for parsing JSON bodies
app.use(express.json());

// Test route
app.get("/", (req, res) => {
  console.log(req); // Logs the request to the console (useful for debugging)
  res.status(200).send("Just changed the hello world to something...");
});

// Use your routes (uncomment if you need specific routes)
app.use(routes); // Assuming routes are properly defined in ./src/routes/Routes.js

// Use the error handler for centralized error handling
app.use(errorHandler);

// Start the server
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

// Optional: Add unhandled promise rejection handler
process.on("unhandledRejection", (error) => {
  console.error("Unhandled Rejection:", error.message);
  // Optionally log the error to a file or monitoring service
});
