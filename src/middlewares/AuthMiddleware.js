var jwt = require("jsonwebtoken");
var bcrypt = require("bcrypt");
const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

async function signup(req, res, next) {
  try {
    const user = {
      name: req.body.name,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8),
    };
    const response = await prisma.User.create({
      data: user,
    });
    res.status(200).json(response);
  } catch (error) {
    next(error);
  }
}

async function signin(req, res) {
  try {
    const user = prisma.findUnique({
      email: req.body.email,
    });

    if (!user) {
      return res.status(404).send({ message: "User Not found" });
    } else {
      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );
      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!",
        });
      }
      var token = jwt.sign(
        {
          id: user.id,
        },
        process.env.API_SECRET,
        {
          expiresIn: 10000,
        }
      );

      res.status(200).send({
        user: {
          id: user.id,
          email: user.email,
          name: user.name,
        },
        message: "Login successfull",
        accessToken: token,
      });
    }
  } catch (error) {
    next(error);
  }
}

module.exports = { signup, signin };
