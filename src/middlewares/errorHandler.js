const errorHandler = (req, res) => {
  res.status(500).json({ message: "Something went wrong..." });
};

module.exports = errorHandler;
