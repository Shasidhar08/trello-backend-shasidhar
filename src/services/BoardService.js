const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const createBoard = async (name, backgroundImage, userId) => {
  try {
    const response = await prisma.board.create({
      data: {
        name,
        backgroundImage,
        userId,
      },
    });
    return response;
  } catch (error) {
    console.log(error.message);
    throw error;
  }
};

const deleteBoard = async (id) => {
  try {
    let response = await prisma.board.delete({
      where: {
        id,
      },
      lists: {
        deleteMany: {},
      },
    });
    return response;
  } catch (error) {
    throw error;
  }
};
const updateBoardName = async (id, name) => {
  try {
    const result = await prisma.board.update({
      where: {
        id,
      },
      data: {
        name,
      },
    });
    return result;
  } catch (error) {
    throw error;
  }
};
const getLists = async (id) => {
  try {
    console.log(id);
    const result = await prisma.board.findUnique({
      where: {
        id,
      },
      select: {
        lists: true,
      },
    });
    return result;
  } catch (error) {
    throw error;
  }
};

// const createBoards = async (req, res) => {
//   try {
//     console.log(req.query.userid);
//     console.log(req.body.data);
//     const { userid } = req.query;
//     const data = await prisma.User.update({
//       where: {
//         id: userid,
//       },
//       data: {
//         boards: {
//           createMany: {
//             data: req.body.data,
//           },
//         },
//       },
//       include: {
//         boards: true,
//       },
//     });
//     res.status(200).json(data);
//   } catch (error) {
//     res.status(500).json({ message: error.message });
//   }
// };

module.exports = { createBoard, deleteBoard, getLists, updateBoardName };
