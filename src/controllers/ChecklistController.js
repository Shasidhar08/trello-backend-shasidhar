const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const createChecklist = async (req, res) => {
  //   console.log(req.query);
  //   console.log(req.params);
  const { name } = req.query;
  const { cardId } = req.params;
  try {
    const response = await prisma.checklist.create({
      data: {
        name,
        cardId,
      },
    });
    console.log(response);
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const getCheckitems = async (req, res) => {
  try {
    let { checklistId } = req.params;
    const result = await prisma.checklist.findUnique({
      where: {
        id: checklistId,
      },
      select: {
        checkitems: true,
      },
    });

    res.status(200).json(result.checkitems);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const deleteChecklist = async (req, res) => {
  try {
    const response = await prisma.checklist.delete({
      where: {
        id: req.params.checklistId,
      },
    });
    console.log(response);
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

module.exports = { createChecklist, getCheckitems, deleteChecklist };
