const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const getBoards = async (req, res, next) => {
  try {
    let { userId } = req.query;
    const result = await prisma.user.findUnique({
      where: {
        id: userId,
      },
      include: {
        boards: true,
      },
    });
    if (result === null) {
      res.status(404).json({ message: "Not Found" });
    } else {
      res.status(200).json(result.boards);
    }
  } catch (error) {
    next(error);
  }
};

const getUsers = async (req, res, next) => {
  try {
    let result = await prisma.user.findMany({
      include: {
        boards: true,
      },
    });
    if (result === null) {
      res.status(404).json({ message: "Not Found" });
    } else {
      res.status(200).json(result);
    }
  } catch (error) {
    next(error);
  }
};

const createUser = async (req, res, next) => {
  try {
    const response = await prisma.user.create({
      data: {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
      },
    });
    res.status(201).json(response);
  } catch (error) {
    next(error);
  }
};

module.exports = { createUser, getUsers, getBoards };

// const getUser = async (req, res, next) => {
//   console.log(req.params.userid);
//   try {
//     let result = await prisma.User.findUnique({
//       where: {
//         id: req.params.userid,
//       },
//       include: {
//         boards: true,
//       },
//     });
//     if (result === null) {
//       res.status(404).json({ message: "Not Found" });
//     } else {
//       res.status(200).json(result);
//     }
//   } catch (error) {
//     next(error);
//   }
// };

// const addBoards = async (req, res, next) => {
//   try {
//     console.log(`userId: ${req.params.userid}`);
//     console.log(req.body.data);
//     let userid = req.params.userid;
//     let result = await prisma.User.update({
//       where: {
//         id: userid,
//       },
//       data: {
//         boards: {
//           createMany: {
//             data: req.body.data,
//           },
//         },
//       },
//     });
//     if (result === null) {
//       res.status(404).json({ message: "Not Found" });
//     } else {
//       res.status(200).json(result);
//     }
//   } catch (error) {
//     next(error);
//   }
// };
