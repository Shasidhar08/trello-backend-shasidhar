const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const createCard = async (req, res) => {
  //   console.log(req.query);
  //   console.log(req.params);
  const { name } = req.query;
  const { listId } = req.params;
  try {
    const response = await prisma.card.create({
      data: {
        name,
        listId,
      },
    });
    console.log(response);
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const getChecklists = async (req, res) => {
  try {
    // console.log("from getLists function");
    // console.log(req.params.listId);
    let { cardId } = req.params;
    const result = await prisma.card.findUnique({
      where: {
        id: cardId,
      },
      select: {
        checklists: true,
      },
    });
    res.status(200).json(result.checklists);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const deleteCard = async (req, res) => {
  try {
    const response = await prisma.card.delete({
      where: {
        id: req.params.cardId,
      },
    });
    console.log(response);
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

module.exports = { createCard, getChecklists, deleteCard };
