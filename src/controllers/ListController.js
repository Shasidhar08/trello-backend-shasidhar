const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const createList = async (req, res) => {
  console.log(req.query);
  const { name } = req.query;
  const { boardId } = req.params;
  try {
    const response = await prisma.list.create({
      data: {
        name,
        boardId,
      },
    });
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const getCards = async (req, res) => {
  try {
    // console.log("from getLists function");
    // console.log(req.params.listId);
    let { listId } = req.params;
    const result = await prisma.list.findUnique({
      where: {
        id: listId,
      },
      select: {
        cards: true,
      },
    });
    if (result === null) {
      res.status(404).json({ message: "Not Found" });
    } else {
      res.status(200).json(result.cards);
    }
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const deleteList = async (req, res) => {
  console.log(req.query);
  try {
    let response = await prisma.list.delete({
      where: {
        id: req.params.listId,
      },
    });
    console.log(response);
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

module.exports = { createList, getCards, deleteList };
