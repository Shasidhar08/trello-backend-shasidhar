const Boards = require("../services/BoardService");
const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const createBoard = async (req, res) => {
  try {
    const { name, userId } = req.query;
    let backgroundImage = `https://picsum.photos/300/200?blur=1&random=${
      Math.floor(Math.random() * 50) + 1
    }`;

    const response = await Boards.createBoard(name, backgroundImage, userId);

    res.status(201).json(response);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const deleteBoard = async (req, res) => {
  try {
    let response = await Boards.deleteBoard(req.params.id);
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const updateBoard = async (req, res) => {
  try {
    let response = await Boards.updateBoardName(
      req.body.boardId,
      req.body.boardName
    );
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const getListsOnBoard = async (req, res) => {
  try {
    let { boardId } = req.params;
    const result = await Boards.getLists(boardId);
    if (result === null) {
      res.status(404).json({ message: "Not Found" });
    } else {
      res.status(200).json(result.lists);
    }
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

module.exports = { createBoard, deleteBoard, getListsOnBoard, updateBoard };
