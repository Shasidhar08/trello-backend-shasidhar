const { PrismaClient, Status } = require("@prisma/client");
const prisma = new PrismaClient();

const createCheckitem = async (req, res) => {
  //   console.log(req.query);
  //   console.log(req.params);
  const { name } = req.query;
  const { checklistId } = req.params;
  try {
    const response = await prisma.checkitem.create({
      data: {
        name,
        checklistId,
      },
    });
    console.log(response);
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const updateCheckitem = async (req, res) => {
  try {
    // console.log(req.query.status)
    let { checkitemId } = req.params;
    let { status } = req.query;
    //  console.log(status);
    console.log(status === "complete");
    const result = await prisma.checkitem.update({
      where: {
        id: checkitemId,
      },
      data: {
        status: status === "complete" ? Status.complete : Status.incomplete,
      },
    });
    console.log(result);
    res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const deleteCheckitem = async (req, res) => {
  try {
    const response = await prisma.checkitem.delete({
      where: {
        id: req.params.checkitemId,
      },
    });
    console.log({ deleted: response });
    res.status(200).json({ DeletedData: response });
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

module.exports = { createCheckitem, updateCheckitem, deleteCheckitem };
