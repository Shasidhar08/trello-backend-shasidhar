const { Router } = require("express");
const UserController = require("../controllers/UserController");
const BoardController = require("../controllers/BoardController");
const ListController = require("../controllers/ListController");
const CardController = require("../controllers/CardController");
const ChecklistController = require("../controllers/ChecklistController");
const CheckitemController = require("../controllers/CheckitemController");

const router = Router();

router.post("/createuser", UserController.createUser);
router.get("/users", UserController.getUsers);
router.get("/users/me", UserController.getBoards);
// router.get("/:userid", UserController.getUser);
// router.put("/:userid", UserController.addBoards);

router.post("/boards", BoardController.createBoard);
router.get("/boards/:boardId/lists", BoardController.getListsOnBoard);
router.delete("/boards/:id", BoardController.deleteBoard);
// router.put("/boards/multiple/:userid", BoardController.createBoards);

router.post("/boards/:boardId/lists", ListController.createList);
router.get("/lists/:listId/cards", ListController.getCards);
router.delete("/lists/:listId/closed", ListController.deleteList);

router.post("/lists/:listId/cards", CardController.createCard);
router.get("/cards/:cardId/checklists", CardController.getChecklists);
router.delete("/cards/:cardId", CardController.deleteCard);

router.post("/cards/:cardId/checklists", ChecklistController.createChecklist);
router.get(
  "/checklists/:checklistId/checkitems",
  ChecklistController.getCheckitems
);
router.delete("/checklists/:checklistId", ChecklistController.deleteChecklist);

router.post(
  "/checklists/:checklistId/checkItems",
  CheckitemController.createCheckitem
);
router.put(
  "/cards/:cardId/checkitem/:checkitemId",
  CheckitemController.updateCheckitem
);
router.delete(
  "/checklists/:checklistId/checkitems/:checkitemId",
  CheckitemController.deleteCheckitem
);

module.exports = router;
